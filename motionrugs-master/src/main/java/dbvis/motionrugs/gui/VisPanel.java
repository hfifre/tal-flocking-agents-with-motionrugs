/*
 * Copyright 2018 Juri Buchmueller <motionrugs@dbvis.inf.uni-konstanz.de>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dbvis.motionrugs.gui;

import dbvis.motionrugs.data.DataPoint;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *  VisPanel contains and transforms the rugs for display in the GUI
 * 
 * @author Juri Buchmüller, University of Konstanz <buchmueller@dbvis.inf.uni-konstanz.de>
 */
public class VisPanel extends JPanel {

    private BufferedImage bf;
    private DataPoint[][] orderedpoints;

    public VisPanel(BufferedImage bf, DataPoint[][] orderedpoints) {
        super();
        this.bf = bf;
        this.orderedpoints = orderedpoints;
    }

    @Override
    public void paintComponent(Graphics g) {
        this.setSize(bf.getWidth(), bf.getHeight());
        this.setPreferredSize(new Dimension(bf.getWidth(), this.getHeight()));

        Graphics2D g2 = (Graphics2D) g;
        AffineTransform af = new AffineTransform();
        af.scale(1, (double) this.getHeight() / bf.getHeight());
        AffineTransformOp afo = new AffineTransformOp(af, AffineTransformOp.TYPE_BICUBIC);

        g2.drawImage(bf, afo, 0, 0);


        //x-axis with frame and time

        g2.setColor(Color.black);
        g2.drawLine(0, bf.getHeight()+5, bf.getWidth(), bf.getHeight()+5);
        int time = (int) this.orderedpoints[0][0].getValue("time");
        double frequencyOfTime = this.orderedpoints[0][0].getValue("frameRate")*2;
        if(frequencyOfTime == 1) frequencyOfTime *= 5;
        boolean alreadyDrawnHere = false;
        for(int i=0;i<bf.getWidth();i++){
            if(time != (int) this.orderedpoints[i][0].getValue("time")) {
                time = (int) this.orderedpoints[i][0].getValue("time");
                double doWeDraw = time%frequencyOfTime;
                if(this.orderedpoints[0][0].getValue("frameRate") == 1){
                    if(doWeDraw==0){
                        g2.drawString(String.valueOf(time), i, bf.getHeight() + 30);
                    }
                }else if(!alreadyDrawnHere) {
                    if (doWeDraw == 0) {
                        g2.drawString(String.valueOf(time), i, bf.getHeight() + 30);
                        alreadyDrawnHere = true;
                    }
                    if (doWeDraw == (frequencyOfTime - 1)) {
                        g2.drawString(String.valueOf(time + 1), i, bf.getHeight() + 30);
                        alreadyDrawnHere = true;
                    }
                    if (doWeDraw == 1) {
                        g2.drawString(String.valueOf(time - 1), i, bf.getHeight() + 30);
                        alreadyDrawnHere = true;
                    }
                }
                if(doWeDraw != 0 && doWeDraw != 1 && doWeDraw != (frequencyOfTime-1) && alreadyDrawnHere) alreadyDrawnHere = false;
            }

            if(i%50 == 0) g2.drawString(String.valueOf(i),i,bf.getHeight()+20);
        }

    }

}
