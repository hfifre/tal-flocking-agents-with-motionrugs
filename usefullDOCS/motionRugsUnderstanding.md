# MotionRugs

## Principe

Le principe est simple : représenter le mouvement du flocking à travers le temps et l'espace.
On veut donc créer un graphe avec comme **x-axis le temps**, et comme **y-axis l'espace**.


![exemple de MotionRugs](imgs/motionRugsExemples.png)

Pour le x-axis, donc le temps, chaque chaque graduation représente une frame.
Pour le y-axis, c'est un peu plus compliqué :

![deroulement](imgs/motionRugsDeroulement.png)

1. Pour chaque frame, on récupère les positions de toutes les entités, et on transforme leur localisation par une structure de données dans l'espace.
2. La structure de données est crée sur la base d'une stratégie déterministe pour dériver un ordre unidimensionnel des emplacements des entités. Cette structure nous donne donc l'ordre des emplacements des entités sur le graphe.
3. On récupère cet ordre pour placer les entités de haut en bas en tant que pixels de couleur sur le y-axis, avec la couleur du pixel correspondant à la données plus ou moins élevé d'une variable de l'entité. (de bleu pour donnée faible à rouge pour donnée élevée)

<div style="page-break-after: always"></div>

## Interpretation visuelle de MotionRugs

#### Spacial stagnation

des lignes parallèles horizontales indique une stagnation globale du groupe.
Cela se passe généralement quand les entités restes dans leur section originale de la structure.

<img src="imgs/spacialStagnation.png" alt="spacial stagnation" width="15%"/>

#### Spacial changes

Un changement spatial fait référence au changement de direction mutuelle du groupe et est représenté par un ruban de lignes qui se courbe de haut en bas ou de bas en haut.

<img src="imgs/spacialChanges.png" alt="spacial changes" width="15%"/>

#### Trend Progression

Un comportement courant dans les collectifs sont des sous-groupes d'entités commençant ou terminant un modèle de comportement. Par exemple un petit groupe d'oiseaux accélère, donc le reste du groupe les suit, et vice-versa.

<img src="imgs/trendProgression.png" alt="trend progression" width="15%"/>

#### Splits & merges

Étant liés aux pattern "trend progression", ils peuvent indiquer des splits & merges spaciales d'entités avec des valeurs de variables similaires. 
D'autre part, un split ne signifie pas une séparation spaciale des groupes d'entités.

<img src="imgs/splits.png" alt="splits" width="15%"/>

<div style="page-break-after: always"></div>

## Differents patterns

![pattern 1 et 2](imgs/pattern1-2.png)

![pattern 3](imgs/pattern3.png)

![pattern 4](imgs/pattern4.png)