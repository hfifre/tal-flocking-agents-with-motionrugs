# Analyse motionRugs avec l'essaim de robots

## SPEED

Le problème de la speed avec motionRugs est qu'elle ne change que très peu dans le projet avec l'essaim de robots. Le graphe n'est pas vraiment analysable, car très peu de changement sont représentés.

## Acceleration