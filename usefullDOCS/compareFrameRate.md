# Comparaison frameRate

| every 5 frames                       | every 10 frames                        | every 1 frames                       |
| ------------------------------------ | -------------------------------------- | ------------------------------------ |
| ![5F](imgs/every5frames.PNG)         | ![10F](imgs/every10frames.PNG)         | ![1F](imgs/every1frames.PNG)         |
| [video 5 frames](videos/5frames.mp4) | [video 10 frames](videos/10frames.mp4) | [video 1 frames](videos/1frames.mp4) |