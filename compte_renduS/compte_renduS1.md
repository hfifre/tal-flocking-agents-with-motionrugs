# COMPTE-RENDU -- 02/05 - 06/05 -- HUGO FIFRE

## TODO générale
- anticiper des anomalies, et surtout pouvoir les visionner avec MotionRugs
- motionRugs a besoin d'un fichier csv pour fonctionner, c'est la qu'il récupère les données
- regarder comment le fichier csv du projet motionRugs est créé et l'adapter au flocking des robots
- étudier le graphe motionRugs :
  - voir s'il correspond bien à ce qu'on veut
  - changer la frequence des frames -> voir quelle frequence utiliser par rapport a la vitesse des robots (les robots sont un peu lent)
- créer le graphe motionRugs en continu (temps réel)

### 02/05 - 9h30-12h, 12h30-17h
- [x] Apprendre ce qu'est le flocking
  - [x] lire documents du fileSender
  - [x] regarder les vidéos
    - [x] [vidéo1](https://www.youtube.com/watch?v=Z8Wf1vF_xgQ&list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl&index=2)
    - [x] [vidéo2](https://www.youtube.com/watch?v=n6h3WiRLmp8&list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl&index=6)
    - [x] mettre en place et tester le modèle de la vidéo ([modèle](http://netlogoweb.org/launch#http://netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Flocking.nlogo))
- [x] Installation de Unity (différentes versions)
- [x] Tuto unity pour se familiariser avec le logiciel

A ce stade j'ai compris le flocking et j'ai commencé à prendre en main unity.

### 03/05 - 8h30-12h, 12h30-16h
- [x] Récupérer le code Unity, tester et étudier le comportement des robots (fichier ReynoldsFlockingAgent.cs)
- [x] Lire et comprendre l'article sur MotionRugs ([vidéo](https://www.youtube.com/watch?v=xSewCmdgN40))
- [x] Réaliser un doc sur ce que j'ai compris de MotionRugs

J'ai pu tester le projet de flocking sous unity (j'ai eu quelques problèmes à comprendre qu'il fallait utiliser un manager et pas mettre les robots sois-même). J'ai lu le code pour comprendre le fonctionnement.
J'ai aussi compris le MotionsRugs et je me suis fait un doc pour m'aider à comprendre et retenir les informations.

<div style="page-break-after: always"></div>

### 04/05 - 8h-12h, 13h-16h
- [x] Test d'un projet de Boids en 3D sous Unity, récupérer sur github ([vidéo](https://youtu.be/bqtqltqcQhw)).
- [x] analyse des fichiers de ce projet.
- [x] analyser les fichiers du projet maven motionRugs
- [x] regarder en details le fichier csv

J'ai voulu tester une autre façon de faire du flocking sous unity (en 3D cette fois). J'ai regardé un peu le code. 
Je suis ensuite passé au projet motionRugs, lu le code pour le comprendre et je me suis penché sur le fichier csv pour voir comment il était créé et utilisé.

### 05/05 - 8h-12h30, 13h-15h30
- [x] tester projet motionRugs
- [x] lire et comprendre l'affichage du motionRugs
- [x] créer un csv depuis le projet unity
- liste des problèmes :
  - probleme avec le projet motionRugs : mauvaise adaptabilité 
  - pas sûr des données distance_centroid et heading_change
  - acceleration non négative 
  - certains 0 au départ
  - petites données comparé au fichier csv de base

J'ai commencé par tester le projet motionRugs, tester l'application et voir les différentes méthodes d'affichage.
Ensuite je me suis lancé dans la création du fichier csv sous unity et c#. Il en est survenu les problèmes que j'ai listé au-dessus.

<div style="page-break-after: always"></div>

### 06/05 - 9h-12h, 12h30-16h30
- [x] résolution des problèmes de la veille
  - [x] mauvaise adaptabilité : il n'y avait pas assez de données, je n'avait pas assez rempli le fichier, mais maintenant ça fonctionne
  - [x] validation des données distance_centroid et heading_change
  - [x] acceleration négative :
    - l'acceleration commence à 0 s'il y en a pas, mais le robot ne peux pas reculer donc ne peux pas avoir une accélération négative
    - comme je n'ai pas l'autre projet avec les poissons, je ne sais pas pourquoi certaines données d'acceleration sont négatives
  - [x] 0 au lancement logique sachant qu'il n'y a pas de vitesse et et changement de direction au début
  - [x] petites données car pas le même projet
    - la map fait que du 20/20
    - les données d'intensités (cohesion, alignement, separation) sont faible

Voici à quoi ressemble l'affichage des données en csv :

| speed                         | acceleration                                | distance_centroid                                     | heading_change                                  |
| ----------------------------- | ------------------------------------------- | ----------------------------------------------------- | ----------------------------------------------- |
| ![speed](imgs/speedOurCSV.png) | ![acceleration](imgs/accelerationOurCSV.png) | ![distance_centroid](imgs/distance_centroidOurCSV.png) | ![heading_change](imgs/heading_changeOurCSV.png) |

J'ai principalement voulu comprendre la source de tous les problèmes en analysant les fichiers (code et csv). Au final tous les problèmes sont assez logique. Le but va etre maintenant d'adapter le projet motionRugs au fichier csv avec des petites données (car pour l'instant ça ne ressemble pas a grand chose), et voir s'il est possible de créer un graphe motionRugs en temps réel.

