# COMPTE-RENDU -- 23/05 - 27/05 -- HUGO FIFRE

## TODO générale
- créer un bash pour comparer les csv
- analyser csv
  - au debut 
  - au fur et a mesure
- seed avec checkbox (avec ou sans seed)
- faire une présentation avec l'analyse des patterns 
- changer les données pour adapter à l'oldCSV
- essayer d'obtenir des patterns quand le mouvement est stabilisé
- algo de tri hilbertCurve

## Semaine du 23/05 - 27/05

### 23/05 - 8h-12h - 13h-16h
- [x] vérifier que la seed donne la même execution à chaque fois
- [x] créer un bash pour comparer les csv et voir la différence pour check le bon fonctionnement de la seed
- [x] faire une vidéo + des graphes avec la seed et étudier le résultat

Pour commencer la semaine j'ai voulu vérifier si la seed était bonne, sauf qu'avec la vitesse des robots c'est très compliquer de le voir à l'oeil nu et pas du tout précis. J'ai donc décidé de faire un bash qui compare différentes executions (grâce aux fichiers csv).
J'ai aussi redirigé l'enregistrement du csv directement dans le projet montionRugs (pour ne pas avoir à le bouger à chaque fois). 
Comme le seed fonctionne très bien, j'ai pu faire une vidéo et étudier les graphes par rapport au comportement de l'essaim de drones. On voit que quand ils se coincent dans un coin de la salle l'acceleration et le changement de direction diminue. à voir si en enlevant de changement que j'ai fait par rapport aux comportement des drones, ça change quelque chose.
J'ai eu aussi une idée pour la création du graphe en continu, il faut que je me tourne surement vers les observers/listeners sous java.

<div style="page-break-after: always"></div>

### 24/05 - 8h-12h45 - 13h15-16h
- [x] Tester différentes execution (changement de vitesse, comportement des robots avec les murs) pour se rapprocher un maximum d'un flocking visible sous motionRugs
- [x] analyser les différentes executions et associer le comportement des robots aux patterns de l'article de recherche
- [x] analyser le fichier csv 
  - [x] vérifier que les données de heading_change et distance_centroid sont cohérentes
    - [x] au début 
    - [x] aux évenements importants
- [x] seed avec checkbox (avec ou sans seed)

J'ai passé la matinée a faire des tests d'execution avec différentes vitesses, en enlevant le fonctionnement bizarre de la collision avec les murs que j'avais fait. 
Après la réunion avec Jéremy Rivière, j'ai analysé le fichier csv pour voir si les données de heading_change et ditance_centroid etaient cohérentes, que se soit au début de l'execution ou dans les evenements importants de l'essaim, et elles le sont.
J'ai un peu eu du mal avec la checkbox pour la seed, alors que c'était au final très simple, du à mon manque d'experience avec Unity.

### 25/05 - 8h30-12h - 13h-17h
- [x] Faire le git
- [x] tester des executions sans shuffle
- [x] régler les problèmes quand on diminue la fréquente de récupération des donnée (1 tout les 5 frames) dû à l'update et FixedUpdate

J'ai commencé par faire le git que j'avais oublié de faire hier, et j'ai fait des tests pour voir si le shuffle ralentissait l'execution du projet unity, ce qui c'est avéré faux (le shuffle ne ralentit pas l'execution).
En voulant tester avec des données plus large (un frameRate de 5, du coup une récupération de données tout les 5 frames), je me suis confronté a un problème que j'ai mit pas mal de temps à régler alors que c'était très simple (je vérifiait si c'était le bon moment pour récupérer les données dans l'Update et non le fixedUpdate, donc j'ai changé ça).

<div style="page-break-after: always"></div>

## Semaine du 30/05 - 03/06

### 30/05 - 8h-12h - 13h30-16h30
- [x] faire des tests pour vérifier que les fonctionnalités fonctionnent bien
- [x] correxion d'une erreur qui ralentissait le déplacement des agents
- [x] tester des executions de la même seed en changeant le frameRate
- [x] affichage des secondes sur une valeur de frameRate > 1
- [ ] réglage de problèmes avec la seed

J'ai commencé la journée par faire des tests avec différentes seeds et différentes fréquence de capture des données. Ce fut une journée de correction de problèmes, mais j'ai aussi pu analyser un peut les résultats et des patterns dans les graphes assez interessant que j'analyserai plus en profondeur demain.
J'ai du coup corriger les problèmes suivants :
- erreur lors du déplacement des agents, que l'on faisait pas a chaque fixedUpdate mais a chaque relevé de données
- affichage des secondes avec un relevé de données supérieur à 1 relevé par frame, dans le projet maven
- reglage des problèmes du random avec la seed, pas encore vérifié si le problème est bien corrigé (a faire demain)

### 31/05 - 9h-12h30 - 13h30-17h

- [x] vérification du réglage de problèmes avec la seed
- [x] tests avec différentes seed (une ou l'on peut voir un groupe unique d'agents)
- [ ] diapo avec l'analyse des différents patterns associés aux graphes et la comparaison avec le résultat attendu, ainsi que les choses qu'on ne retrouve pas du projet motionRugs de base

### 01/06 - 8h-12h30 - 13h30-16h
- [x] finir gifs pour la présentation
- [X] analyser le csv (faire min et max des données)
- [x] retrouver l'explication sur les features dans le doc | le git de motionrugs

Au petit matin je n'avais pas fini de mettre tous les relevés de patterns en gifs, j'ai donc fini ça.
J'ai après fait en sorte que l'on puisse voir le min et le max de x et y pour tous les agents à n'importe quel moment.
pareil pour features.
J'ai enfin relu l'article de recherche pour trouvé des infos sur heading_change et distance_centroid (il n'y en a pas beaucoup).

<div style="page-break-after: always"></div>

### 02/06 - 8h30-13h - 13h30-16h
- [x] tester en enlevant le x1000 sur toutes les valeurs du csv
- [x] redimentionner la map selon l'oldCSV (calculé en mm/s)
- [x] essayer de changer le distance_centroid et heading_change pour avoir des valeurs similaires au csv de base

Pour les anciennes simulations, je multipliais les données par 1000 pour avoir les mêmes données que sur l'oldCSV, car eux calculait ces données en mm/s. J'ai enlevé ça et normalisé un peu toutes les données ainsi que la map. Grace au min et au max j'ai pu voir quelle taille avait la map : x est a 1800mm et y à 1100mm. Pour avoir le même resultat, on a juste a faire une map de 18 sur 11, et a multiplier par 100 pour avoir des données cohérentes avec l'oldCSV (au niveau du x et y).
J'ai aussi essayé de changer les données de distance_centroid et heading_change pour qu'elles correspondent plus à ce qu'on veut (pas les forces par rapport au intensités mais les forces directements calculées). J'ai en plus de ça changé les intensités pour que le mouvement des agents forme plus du flocking

### 03/06 - 8h-13h - 13h30-15h30
- [x] changement de la taille de la map (pour se rapprocher du comportement voulu)
- [x] tester différentes fréquences de relevés de données
- [x] faire algo (pseudo-code) pour mettre des couleurs sur les points en faisant l'algo d'hilbert curve sous unity

J'ai commencé la journée par lancer une simulation et voir le comportement des agents. malheureusement ce n'était pas vraiment ce que je voulais, donc je me suis dis qu'en reduisant la taille de la map le comportement ressemblerait plus à celui du projet motionRugs de base.
J'ai pu voir qu'avec des petits relevés de données on voit quand même des formes interessantes. A analyser lundi.
J'ai fait un algo en pseudo code a implémenter pour pouvoir trier sous unity les agents en hilbertCurve. a faire aussi ce week-end.
