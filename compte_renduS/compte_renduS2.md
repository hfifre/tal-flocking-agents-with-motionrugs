# COMPTE-RENDU -- 09/05 - 13/05 -- HUGO FIFRE

## TODO générale
- afficher en abscisse le temps en frames et en secondes
- faire des captures vidéos pour comparer le fonctionnement sous unity et motionRugs
- régler les problèmes de valeurs non correspondantes entre ces deux projets

### 09/05 - 8h-12h, 13h-16h
- [ ] afficher temps en abscisse sur le graphe (projet maven)
- [x] capture vidéo pour comparaison avec le graphe
- [x] changer enregistrement des frames

J'ai essayé toute la matinée d'afficher les abscisse sans grande réussite, car le graphe est en fait une image affichée et que je ne m'y connais pas (encore) assez avec swing pour trouver la solution. Je suis donc passé dans l'aprem sur le changement d'enregistrement pour chaque frame car l'enregistrement etait en fait mal fait donc pas optimisé. J'ai ensuite fait des captures vidéos pour différents framerates (un enregistrement tout les 5, 10, 1 frames).

### 10/05 - 8h-12h, 12h30-15h30
- [x] afficher temps en abscisse sur le graphe (projet maven)
- [x] rendre l'acceleration valide
- [x] changer theorème de pythagore en fonction du vector3
- [x] position du projet unity à (0,0) : en bas à gauche

J'ai résolu le problème pour afficher le temps sur le graphe, mais ça ne change pas (encore) d'échelle quand on fait un enregistrement tout les x frames (x>1). Je verrai ça demain. J'ai refait l'accéleration pour ne pas qu'elle soit à 0.0 (problème avec l'updateAgent qui reinitialisait la valeur). J'ai enlevé le calcul de la tangente aussi pour la remplacer par une fonction de vector3 qui renvoie sa longueur. J'ai aussi bien vérifié qui la position du projet unity commence à (0,0).

### 11/05 - 8h30-12h, 12h30-16h
- [x] mettre le framerate dans le csv 
- [x] recupérer le framerate du csv pour adapter l'axe des abscisses à la bonne échelle
- [x] étudier les unités de valeur de motionRugs (chercher dans l'article) : unité en mm/s
- [x] changement de l'unité des valeurs speed, acceleration, distance_centroid et heading_change en mm/s 

unity est en unity unit / second donc si on defini une unité de unity en tant qu'un metre, il faut multiplier par 1000 pour avoir la valeur en mm/s. Cela donne des valeurs trop grandes et peu adapté a mes yeux au projet unity, car une unité de unity se rapproche plus d'un centimètre que d'un metre. J'ai donc multiplié par 100.
J'ai aussi passé la vitesse max à 2 pour avoir plus de variation, mais elle reste bloqué au max (ce qui est logique en soit sachant que sous le projet unity on l'augmente à chaque fois, jusqu'à ce qu'elle arrive à son max).
Le fichier ressemble dejà plus à celui de base du projet motionRugs, mais les graphes sont toujours différents (ce qui s'explique par le fait que dans le projet unity les agents rebondissent sur le mur au lieu de les éviter).

### 12/05 - 8h-12h, 13h-16h
- [ ] Changer le projet unity pour faire en sorte que les agents évitent les murs  

J'ai voulu faire en sorte que les agents aient le même comportement que les poisson du projet de base de MotionRugs, mais comme c'est un manager qui gère tous les objets ça a été plus dur. Je me suis appuyé d'une vidéo ou il explique comment il fait pour contourner des objets, mais j'ai bien l'impression que le MeshCollider que j'ai mit sur la map ne fonctionne pas ou que la fonction permettant de rebondir sur les murs ne fonctionne pas, je regarderai ça demain.

### 13/05 - 8h30-1230h, 13h-16h
- [x] abandon de changement du projet Unity (mauvaise piste) donc suppression des modifications
- [x] vérifier le frames/s sous le projet unity
- [x] changer l'abscisse 
  - [x] changer en frames
  - [x] changer en scondes
- [x] faire parallèle entre une vidéo et une modèlisation motionRugs pour voir si le fonctionnement est bon

J'ai commencé par enlever toutes les modifications que j'avais fait la veille car j'étais parti vers une mauvaise piste. 
J'ai ensuite voulu voir combien le projet unity avait de frames par seconde pour avoir un meilleur visuel entre la vidéo de simulation et le graphe motionRugs. Malheureusement il y a trop de changements entre 2 executions (causés par l'état de mon ordinateur au lancement de l'execution surtout).
J'ai donc laissé l'affichage des frames tout les 50frames sur l'abscisse, en rajoutant en dessous les secondes. 
J'ai fait une vidéo (avec un framerate de 1) et j'ai pris des photos pour comparer simulation et graphe motionRugs.