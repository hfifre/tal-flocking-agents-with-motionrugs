# COMPTE-RENDU -- 16/05 - 20/05 -- HUGO FIFRE

## TODO générale
- creer un seed pour faire du random controlé
- créer un meilleur comportement des agents au niveau de la collision des murs
- créer un FixedUpdate pour ne plus avoir de variation entre chaque execution a cause du temps entre chaque Update
- shuffle la liste d'agents à chaque FixedUpdate
- essayer de normaliser la speed sous motionRugs pour ne plus que le graphe soit bleu

### 16/05 - 8h-12h, 13h-16h
- [x] créer un champ *seed* sous unity pour réaliser la même simulation à chaque execution
- [x] fluidifier les collisions
- [ ] faire vidéo de la seed

Aujourd'hui j'ai créé un champs *seed* pour pouvoir garder le même seed et faire des comparaison entre le fait de récupérer toutes les frames, 5 frames ou 10 frames des données.
Je suis passé sur la fluidification des collisions. Je n'ai pas réussi a les fluidifier comme je le voulais (éviter les murs), mais j'ai quand même donné un mouvement au groupe.
J'ai voulu ensuite faire les graphes de la seed en 1, 5 et 10 frames mais malheureusement j'ai remarqué qu'il y avait aussi des mouvements randoms et que même en enlevant la fonction *randomMovement()* les mouvements des agents n'étaient pas les mêmes d'une execution à une autre.

### 17/05 - 8h-12h, 13h-16h
- [x] faire fonctionner correctement la seed
- [x] faire une vidéo de la seed
- [x] faire des relevés avec différentes fréquences d'enregistrement
- [x] créer le fixeUpdate avec les fonctions decide et act dans l'agent
- [ ] fluidifier l'execution

La seed fonctionne correctement, j'ai encore pas mal de choses a régler quand même pour avoir une execution aléatoire par rapport au seed (faire le pointeur vers la seed pour chaque agent). J'ai fait une vidéo de la seed avec les relevés de fréquences avant tous les changements a faire. 
J'ai remplacé les fonctions *update* et *lateUpdate* de *ReynoldsFlockingAgent*, et je les lance directement depuis *AgentManager*. Malheureusement la fluidité est vraiment moins bonnes, ce qui est logique car au lieu de laisser chaque agent gérer son déplacement, c'est le manager qui gère TOUS les déplacements des agents. J'ai du boulot pour demain !

<div style="page-break-after: always"></div>

### 18/05 - 8h-12h, 12h30-15h30
- [x] faire un pointeur vers la seed pour chaque agent (pour que chaque agent agissent differemment avec la même seed)
- [ ] essayer d'arranger les problèmes avec le fixeupdate

J'ai voulu faire le pointeur pour récupérer la seed, mais j'ai du changer le random (qui était le UnityEngine.Random) par System.Random pour pouvoir le réutiliser dans la classe de l'agent (ReynoldsFlockingAgent). 
J'ai voulu retourner sur le problème de fixeupdate sans grande avancé. C'est un peu compliqué...

### 19/05 - 8h-12h, 13h-16h
- [x] resolution des problèmes de performances avec le fixedUpdate (partager Update et FixedUpdate)
- [x] résolution des erreur de references null (ajout d'une fonction remplaçant le start)
- [x] essayer de shuffle les agents avec la seed à chaque fixedUpdate

Le problème du fixedUpdate venait du fait que l'on faisait l'écriture du CSV dedans avec le mouvement des agents. J'ai donc mit l'écriture dans l'Update et les performances ont grandement augmentés. 
L'erreur de references null avec le fixedUpdate venait du fait que la fonction *Start()* de la classe *ReynoldsFlockingAgent* ne se lançait pas à la création d'un agent, donc j'ai rajouté une classe qui fait la même chose que le Start. j'ai aussi instancié le System.Random dans *AgentManager* et fait un get depuis *ReynoldsFlockingAgent* plutot que faire un set, sinon il y avait encore des problèmes de références nulls.
Il n'y a pas de fonction de shuffle sous unity, donc la seule façon que j'ai trouvé est de parcourir le tableau et de la shuffle à la main avec le randomRange.

### 20/05 - 8h-12h, 13h-16h
- [ ] normaliser la speed sur motionRugs (faire que la couleur soit rouge)
- [x] résouvre les problèmes de shuffle

J'ai regarder comment normaliser le speed depuis le projet Maven sur motionRugs. Après avoir compris comment les couleurs étaient déterminées (avec des quantiles), j'ai remarqué que les données étaient déjà normalisées. Malheureusement vu que la speed à très peu de variations le quantiles ne prendre que les valeurs les plus communes (il met une variation de 39.99 à 40.00), d'ou la faible variation de couleurs.
Il y avait des problèmes avec le shuffle, car vu que c'était dans le FixedUpdate alors que l'écriture etait dans l'Update, les id n'étaient pas bons. J'instancie donc le string à écrire, le remplis dans le FixedUpdate et je ne fais que l'écriture dans l'Update.