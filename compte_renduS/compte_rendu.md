# COMPTE-RENDU

## Semaine du 02/05 - 06/05

### 02/05 - 9h30-12h, 12h30-17h
- [x] Apprendre ce qu'est le flocking
  - [x] lire documents du fileSender
  - [x] regarder les vidéos
    - [x] [vidéo1](https://www.youtube.com/watch?v=Z8Wf1vF_xgQ&list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl&index=2)
    - [x] [vidéo2](https://www.youtube.com/watch?v=n6h3WiRLmp8&list=PLF0b3ThojznRKYcrw8moYMUUJK2Ra8Hwl&index=6)
    - [x] mettre en place et tester le modèle de la vidéo ([modèle](http://netlogoweb.org/launch#http://netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Flocking.nlogo))
- [x] Installation de Unity (différentes versions)
- [x] Tuto unity pour se familiariser avec le logiciel

A ce stade j'ai compris le flocking et j'ai commencé à prendre en main unity.

### 03/05 - 8h30-12h, 12h30-16h
- [x] Récupérer le code Unity, tester et étudier le comportement des robots (fichier ReynoldsFlockingAgent.cs)
- [x] Lire et comprendre l'article sur MotionRugs ([vidéo](https://www.youtube.com/watch?v=xSewCmdgN40))
- [x] Réaliser un doc sur ce que j'ai compris de MotionRugs

J'ai pu tester le projet de flocking sous unity (j'ai eu quelques problèmes à comprendre qu'il fallait utiliser un manager et pas mettre les robots sois-même). J'ai lu le code pour comprendre le fonctionnement.
J'ai aussi compris le MotionsRugs et je me suis fait un doc pour m'aider à comprendre et retenir les informations.

### 04/05 - 8h-12h, 13h-16h
- [x] Test d'un projet de Boids en 3D sous Unity, récupérer sur github ([vidéo](https://youtu.be/bqtqltqcQhw)).
- [x] analyse des fichiers de ce projet.
- [x] analyser les fichiers du projet maven motionRugs
- [x] regarder en details le fichier csv

J'ai voulu tester une autre façon de faire du flocking sous unity (en 3D cette fois). J'ai regardé un peu le code. 
Je suis ensuite passé au projet motionRugs, lu le code pour le comprendre et je me suis penché sur le fichier csv pour voir comment il était créé et utilisé.

### 05/05 - 8h-12h30, 13h-15h30
- [x] tester projet motionRugs
- [x] lire et comprendre l'affichage du motionRugs
- [x] créer un csv depuis le projet unity
- liste des problèmes :
  - probleme avec le projet motionRugs : mauvaise adaptabilité 
  - pas sûr des données distance_centroid et heading_change
  - acceleration non négative 
  - certains 0 au départ
  - petites données comparé au fichier csv de base

J'ai commencé par tester le projet motionRugs, tester l'application et voir les différentes méthodes d'affichage.
Ensuite je me suis lancé dans la création du fichier csv sous unity et c#. Il en est survenu les problèmes que j'ai listé au-dessus.

### 06/05 - 9h-12h, 13h-17h
- [x] résolution des problèmes de la veille
  - [x] mauvaise adaptabilité : il n'y avait pas assez de données, je n'avait pas assez rempli le fichier, mais maintenant ça fonctionne
  - [x] validation des données distance_centroid et heading_change
  - [x] acceleration négative :
    - l'acceleration commence à 0 s'il y en a pas, mais le robot ne peux pas reculer donc ne peux pas avoir une accélération négative
    - comme je n'ai pas l'autre projet avec les poissons, je ne sais pas pourquoi certaines données d'acceleration sont négatives
  - [x] 0 au lancement logique sachant qu'il n'y a pas de vitesse et et changement de direction au début
  - [x] petites données car pas le même projet
    - la map fait que du 20/20
    - les données d'intensités (cohesion, alignement, separation) sont faible

J'ai principalement voulu comprendre la source de tous les problèmes en analysant les fichiers (code et csv). Au final tous les problèmes sont assez logique. Le but va etre maintenant d'adapter le projet motionRugs au fichier csv avec des petites données, et voir s'il est possible de créer un graphe motionRugs en temps réel.

## Semaine du 09/05 - 13/05

### 09/05 - 8h-12h, 13h-16h
- [ ] afficher temps en abscisse sur le graphe (projet maven)
- [x] capture vidéo pour comparaison avec le graphe
- [x] changer enregistrement des frames

J'ai essayé toute la matinée d'afficher les abscisse sans grande réussite, car le graphe est en fait une image affichée et que je ne m'y connais pas (encore) assez avec swing pour trouver la solution. Je suis donc passé dans l'aprem sur le changement d'enregistrement pour chaque frame car l'enregistrement etait en fait mal fait donc pas optimisé. J'ai ensuite fait des captures vidéos pour différents framerates (un enregistrement tout les 5, 10, 1 frames).

### 10/05 - 8h-12h, 12h30-15h30
- [x] afficher temps en abscisse sur le graphe (projet maven)
- [x] rendre l'acceleration valide
- [x] changer theorème de pythagore en fonction du vector3
- [x] position du projet unity à (0,0) : en bas à gauche

J'ai résolu le problème pour afficher le temps sur le graphe, mais ça ne change pas (encore) d'échelle quand on fait un enregistrement tout les x frames (x>1). Je verrai ça demain. J'ai refait l'accéleration pour ne pas qu'elle soit à 0.0 (problème avec l'updateAgent qui reinitialisait la valeur). J'ai enlevé le calcul de la tangente aussi pour la remplacer par une fonction de vector3 qui renvoie sa longueur. J'ai aussi bien vérifié qui la position du projet unity commence à (0,0).

### 11/05 - 8h30-12h, 12h30-16h
- [x] mettre le framerate dans le csv 
- [x] recupérer le framerate du csv pour adapter l'axe des abscisses à la bonne échelle
- [x] étudier les unités de valeur de motionRugs (chercher dans l'article) : unité en mm/s
- [x] changement de l'unité des valeurs speed, acceleration, distance_centroid et heading_change en mm/s 

unity est en unity unit / second donc si on defini une unité de unity en tant qu'un metre, il faut multiplier par 1000 pour avoir la valeur en mm/s. Cela donne des valeurs trop grandes et peu adapté a mes yeux au projet unity, car une unité de unity se rapproche plus d'un centimètre que d'un metre. J'ai donc multiplié par 100.
J'ai aussi passé la vitesse max à 2 pour avoir plus de variation, mais elle reste bloqué au max (ce qui est logique en soit sachant que sous le projet unity on l'augmente à chaque fois, jusqu'à ce qu'elle arrive à son max).
Le fichier ressemble dejà plus à celui de base du projet motionRugs, mais les graphes sont toujours différents (ce qui s'explique par le fait que dans le projet unity les agents rebondissent sur le mur au lieu de les éviter).

### 12/05 - 8h-12h, 13h-16h
- [ ] Changer le projet unity pour faire en sorte que les agents évitent les murs  

J'ai voulu faire en sorte que les agents aient le même comportement que les poisson du projet de base de MotionRugs, mais comme c'est un manager qui gère tous les objets ça a été plus dur. Je me suis appuyé d'une vidéo ou il explique comment il fait pour contourner des objets, mais j'ai bien l'impression que le MeshCollider que j'ai mit sur la map ne fonctionne pas ou que la fonction permettant de rebondir sur les murs ne fonctionne pas, je regarderai ça demain.

### 13/05 - 8h30-12h30, 13h-16h
- [x] abandon de changement du projet Unity (mauvaise piste) donc suppression des modifications
- [x] vérifier le frames/s sous le projet unity
- [x] changer l'abscisse 
  - [x] changer en frames
  - [x] changer en scondes
- [x] faire parallèle entre une vidéo et une modèlisation motionRugs pour voir si le fonctionnement est bon

J'ai commencé par enlever toutes les modifications que j'avais fait la veille car j'étais parti vers une mauvaise piste. 
J'ai ensuite voulu voir combien le projet unity avait de frames par seconde pour avoir un meilleur visuel entre la vidéo de simulation et le graphe motionRugs. Malheureusement il y a trop de changements entre 2 executions (causés par l'état de mon ordinateur au lancement de l'execution surtout).
J'ai donc laissé l'affichage des frames tout les 50frames sur l'abscisse, en rajoutant en dessous les secondes. 
J'ai fait une vidéo (avec un framerate de 1) et j'ai pris des photos pour comparer simulation et graphe motionRugs.

## Semaine du 16/05 - 20/05

### 16/05 - 8h-12h, 13h-16h
- [x] créer un champ *seed* sous unity pour réaliser la même simulation à chaque execution
- [x] fluidifier les collisions
- [ ] faire vidéo de la seed

Aujourd'hui j'ai créé un champs *seed* pour pouvoir garder le même seed et faire des comparaison entre le fait de récupérer toutes les frames, 5 frames ou 10 frames des données.
Je suis passé sur la fluidification des collisions. Je n'ai pas réussi a les fluidifier comme je le voulais (éviter les murs), mais j'ai quand même donné un mouvement au groupe.
J'ai voulu ensuite faire les graphes de la seed en 1, 5 et 10 frames mais malheureusement j'ai remarqué qu'il y avait aussi des mouvements randoms et que même en enlevant la fonction *randomMovement()* les mouvements des agents n'étaient pas les mêmes d'une execution à une autre.

### 17/05 - 8h-12h, 13h-16h
- [x] faire fonctionner correctement la seed
- [x] faire une vidéo de la seed
- [x] faire des relevés avec différentes fréquences d'enregistrement
- [x] créer le fixeUpdate avec les fonctions decide et act dans l'agent
- [ ] fluidifier l'execution

La seed fonctionne correctement, j'ai encore pas mal de choses a régler quand même pour avoir une execution aléatoire par rapport au seed (faire le pointeur vers la seed pour chaque agent). J'ai fait une vidéo de la seed avec les relevés de fréquences avant tous les changements a faire. 
J'ai remplacé les fonctions *update* et *lateUpdate* de *ReynoldsFlockingAgent*, et je les lance directement depuis *AgentManager*. Malheureusement la fluidité est vraiment moins bonnes, ce qui est logique car au lieu de laisser chaque agent gérer son déplacement, c'est le manager qui gère TOUS les déplacements des agents. J'ai du boulot pour demain !

### 18/05 - 8h-12h, 12h30-15h30
- [x] faire un pointeur vers la seed pour chaque agent (pour que chaque agent agissent differemment avec la même seed)
- [ ] essayer d'arranger les problèmes avec le fixeupdate

J'ai voulu faire le pointeur pour récupérer la seed, mais j'ai du changer le random (qui était le UnityEngine.Random) par System.Random pour pouvoir le réutiliser dans la classe de l'agent (ReynoldsFlockingAgent). 
J'ai voulu retourner sur le problème de fixeupdate sans grande avancé. C'est un peu compliqué...

### 19/05 - 8h-12h, 13h-16h
- [x] resolution des problèmes de performances avec le fixedUpdate (partager Update et FixedUpdate)
- [x] résolution des erreur de references null (ajout d'une fonction remplaçant le start)
- [x] essayer de shuffle les agents avec la seed à chaque fixedUpdate

Le problème du fixedUpdate venait du fait que l'on faisait l'écriture du CSV dedans avec le mouvement des agents. J'ai donc mit l'écriture dans l'Update et les performances ont grandement augmentés. 
L'erreur de references null avec le fixedUpdate venait du fait que la fonction *Start()* de la classe *ReynoldsFlockingAgent* ne se lançait pas à la création d'un agent, donc j'ai rajouté une classe qui fait la même chose que le Start. j'ai aussi instancié le System.Random dans *AgentManager* et fait un get depuis *ReynoldsFlockingAgent* plutot que faire un set, sinon il y avait encore des problèmes de références nulls.
Il n'y a pas de fonction de shuffle sous unity, donc la seule façon que j'ai trouvé est de parcourir le tableau et de la shuffle à la main avec le randomRange.

### 20/05 - 8h-12h, 13h-16h
- [ ] normaliser la speed sur motionRugs (faire que la couleur soit rouge)
- [x] résouvre les problèmes de shuffle

J'ai regarder comment normaliser le speed depuis le projet Maven sur motionRugs. Après avoir compris comment les couleurs étaient déterminées (avec des quantiles), j'ai remarqué que les données étaient déjà normalisées. Malheureusement vu que la speed à très peu de variations le quantiles ne prendre que les valeurs les plus communes (il met une variation de 39.99 à 40.00), d'ou la faible variation de couleurs.
Il y avait des problèmes avec le shuffle, car vu que c'était dans le FixedUpdate alors que l'écriture etait dans l'Update, les id n'étaient pas bons. J'instancie donc le string à écrire, le remplis dans le FixedUpdate et je ne fais que l'écriture dans l'Update.

## Semaine du 23/05 - 27/05

### 23/05 - 8h-12h - 13h-16h
- [x] vérifier que la seed donne la même execution à chaque fois
- [x] créer un bash pour comparer les csv et voir la différence pour check le bon fonctionnement de la seed
- [x] faire une vidéo + des graphes avec la seed et étudier le résultat

Pour commencer la semaine j'ai voulu vérifier si la seed était bonne, sauf qu'avec la vitesse des robots c'est très compliquer de le voir à l'oeil nu et pas du tout précis. J'ai donc décidé de faire un bash qui compare différentes executions (grâce aux fichiers csv).
J'ai aussi redirigé l'enregistrement du csv directement dans le projet montionRugs (pour ne pas avoir à le bouger à chaque fois). 
Comme le seed fonctionne très bien, j'ai pu faire une vidéo et étudier les graphes par rapport au comportement de l'essaim de drones. On voit que quand ils se coincent dans un coin de la salle l'acceleration et le changement de direction diminue. à voir si en enlevant de changement que j'ai fait par rapport aux comportement des drones, ça change quelque chose.
J'ai eu aussi une idée pour la création du graphe en continu, il faut que je me tourne surement vers les observers/listeners sous java.

### 24/05 - 8h-12h45 - 13h15-16h
- [x] Tester différentes execution (changement de vitesse, comportement des robots avec les murs) pour se rapprocher un maximum d'un flocking visible sous motionRugs
- [x] analyser les différentes executions et associer le comportement des robots aux patterns de l'article de recherche
- [x] analyser le fichier csv 
  - [x] vérifier que les données de heading_change et distance_centroid sont cohérentes
    - [x] au début 
    - [x] aux évenements importants
- [x] seed avec checkbox (avec ou sans seed)

J'ai passé la matinée a faire des tests d'execution avec différentes vitesses, en enlevant le fonctionnement bizarre de la collision avec les murs que j'avais fait. 
Après la réunion avec Jéremy Rivière, j'ai analysé le fichier csv pour voir si les données de heading_change et ditance_centroid etaient cohérentes, que se soit au début de l'execution ou dans les evenements importants de l'essaim, et elles le sont.
J'ai un peu eu du mal avec la checkbox pour la seed, alors que c'était au final très simple, du à mon manque d'experience avec Unity.

### 25/05 - 8h30-12h - 13h-17h
- [x] Faire le git
- [x] tester des executions sans shuffle
- [x] régler les problèmes quand on diminue la fréquente de récupération des donnée (1 tout les 5 frames) dû à l'update et FixedUpdate

J'ai commencé par faire le git que j'avais oublié de faire hier, et j'ai fait des tests pour voir si le shuffle ralentissait l'execution du projet unity, ce qui c'est avéré faux (le shuffle ne ralentit pas l'execution).
En voulant tester avec des données plus large (un frameRate de 5, du coup une récupération de données tout les 5 frames), je me suis confronté a un problème que j'ai mit pas mal de temps à régler alors que c'était très simple (je vérifiait si c'était le bon moment pour récupérer les données dans l'Update et non le fixedUpdate, donc j'ai changé ça).

## Semaine du 30/05 - 03/06

### 30/05 - 8h-12h - 13h30-16h30
- [x] faire des tests pour vérifier que les fonctionnalités fonctionnent bien
- [x] correxion d'une erreur qui ralentissait le déplacement des agents
- [x] tester des executions de la même seed en changeant le frameRate
- [x] affichage des secondes sur une valeur de frameRate > 1
- [ ] réglage de problèmes avec la seed

J'ai commencé la journée par faire des tests avec différentes seeds et différentes fréquence de capture des données. Ce fut une journée de correction de problèmes, mais j'ai aussi pu analyser un peut les résultats et des patterns dans les graphes assez interessant que j'analyserai plus en profondeur demain.
J'ai du coup corriger les problèmes suivants :
- erreur lors du déplacement des agents, que l'on faisait pas a chaque fixedUpdate mais a chaque relevé de données
- affichage des secondes avec un relevé de données supérieur à 1 relevé par frame, dans le projet maven
- reglage des problèmes du random avec la seed, pas encore vérifié si le problème est bien corrigé (a faire demain)

### 31/05 - 9h-12h30 - 13h30-17h

- [x] vérification du réglage de problèmes avec la seed
- [x] tests avec différentes seed (une ou l'on peut voir un groupe unique d'agents)
- [ ] diapo avec l'analyse des différents patterns associés aux graphes et la comparaison avec le résultat attendu, ainsi que les choses qu'on ne retrouve pas du projet motionRugs de base

### 01/06 - 8h-12h30 - 13h30-16h
- [x] finir gifs pour la présentation
- [X] analyser le csv (faire min et max des données)
- [x] retrouver l'explication sur les features dans le doc | le git de motionrugs

Au petit matin je n'avais pas fini de mettre tous les relevés de patterns en gifs, j'ai donc fini ça.
J'ai après fait en sorte que l'on puisse voir le min et le max de x et y pour tous les agents à n'importe quel moment.
pareil pour features.
J'ai enfin relu l'article de recherche pour trouvé des infos sur heading_change et distance_centroid (il n'y en a pas beaucoup).

### 02/06 - 8h30-13h - 13h30-16h
- [x] tester en enlevant le x1000 sur toutes les valeurs du csv
- [x] redimentionner la map selon l'oldCSV (calculé en mm/s)
- [x] essayer de changer le distance_centroid et heading_change pour avoir des valeurs similaires au csv de base

Pour les anciennes simulations, je multipliais les données par 1000 pour avoir les mêmes données que sur l'oldCSV, car eux calculait ces données en mm/s. J'ai enlevé ça et normalisé un peu toutes les données ainsi que la map. Grace au min et au max j'ai pu voir quelle taille avait la map : x est a 1800mm et y à 1100mm. Pour avoir le même resultat, on a juste a faire une map de 18 sur 11, et a multiplier par 100 pour avoir des données cohérentes avec l'oldCSV (au niveau du x et y).
J'ai aussi essayé de changer les données de distance_centroid et heading_change pour qu'elles correspondent plus à ce qu'on veut (pas les forces par rapport au intensités mais les forces directements calculées). J'ai en plus de ça changé les intensités pour que le mouvement des agents forme plus du flocking

### 03/06 - 8h-13h - 13h30-15h30
- [x] changement de la taille de la map (pour se rapprocher du comportement voulu)
- [x] tester différentes fréquences de relevés de données
- [x] faire algo (pseudo-code) pour mettre des couleurs sur les points en faisant l'algo d'hilbert curve sous unity

J'ai commencé la journée par lancer une simulation et voir le comportement des agents. malheureusement ce n'était pas vraiment ce que je voulais, donc je me suis dis qu'en reduisant la taille de la map le comportement ressemblerait plus à celui du projet motionRugs de base.
J'ai pu voir qu'avec des petits relevés de données on voit quand même des formes interessantes. a analyser lundi (même si c'est ferié ^^').
J'ai fait un algo en pseudo code a implémenter pour pouvoir trier sous unity les agents en hilbertCurve. a faire aussi ce week-end.

## Semaine du 30/05 - 03/06

### 07/05 - 8h-12h30 - 13h30-16h
- [x] commencer le rapport (intro, présentation Lab-STICC, pole interaction et equipe INUIT)
- [ ] faire un programme en c++ pour faire des graphes
- [x] faire des graphes avec le csv depuis un site

J'ai commencé la journée par commencer le rapport (avant la réunion avec Mr Rivière). Il y a eu la réunion après laquelle je me suis mit dans la tête de faire un programme pour afficher des graphes avec les fichiers csv.
J'ai réfléchi pendant le repas et je me suis dis que ce serait un peu trop long (bah oui...) donc j'ai juste importé mes fichiers csv sur un site qui s'en charge et j'ai pris des screens.
J'ai remarqué avec ces graphes des problèmes : 
- le données sont très différentes de ce qu'on est sensé avec avec le csv exemple
- le shuffle est mauvais pour les graphes, on a des coordonnées qui n'ont pas de sens

### 08/05 - 8h-12h30 - 13h30-16h
- [x] faire tests sans le shuffle
- [x] prendre des screens des bons graphes (avec un frameRate de 10)
- [x] relancer la simulation + motionRugs et analyser le graphes par rapport aux gifs (patterns)
- [x] continuer le rapport (partie contexte : flocking et motionRugs)

J'ai commencé la journée par tester la simulation sans le shuffle : c'était magique ! enfin le résultat attendu.
J'ai donc refait tous les graphes, avec un frameRate de 10 aussi pour voir plus de choses, et on voit pleins de patterns. J'ai donc relevé les patterns en comparant les graphes de distance_centroid (le plus interessant) et heading_change avec les gifs de le vidéo de simulation.
Avec ça de fait, j'ai continué le rapport qui va m'aider a faire mon diapo de présentation pour vendredi (soutenance blanche).

## TODO générale
- créer le graphe motionRugs en continu (temps réel)
- 1uu = 1m --> speed en m.s

- faire graphe avec un agent (plot)
- rapport
  - coherence des données
  - recherches
  - graphes de cohérence des données
  - unités choisi
  - calculs des données
  - enregistrement csv
  - choix fréquence enregistrement
  - resultats : patterns, captures vidéos
  - comparaison avec les patterns de base (ressemblance et nouveaux)