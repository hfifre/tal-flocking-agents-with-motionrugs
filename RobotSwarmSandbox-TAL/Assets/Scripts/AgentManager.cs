using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UIElements;

public class AgentManager : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private GameObject mapPrefab;

    [SerializeField]
    private int numberOfAgents = 40;

    [SerializeField]
    [Range(1, 20)]
    private float mapSizeX = 5.0f;

    [SerializeField]
    [Range(1, 20)]
    private float mapSizeZ = 5.0f;

    [SerializeField]
    [Range(1, 60)]
    private int frameRate = 1;

    [SerializeField]
    [Range(1, 100)]
    public int seed = 1;

    [SerializeField]
    private bool seedActive;

    private Camera mainCamera;

    private System.Random random;
    private string path;
    private string adding = "";

    private double timeStart;

    private List<GameObject> agents;
    // Start is called before the first frame update
    void Start()
    {
        this.timeStart = System.Math.Truncate(Time.realtimeSinceStartup);

        // path to csv file
        int k = 0;
        //while (File.Exists(Application.dataPath + "/csv/fileTestNum" + k + ".csv"))
        while (File.Exists(Application.dataPath + "../../../motionRugs-master/data/projetUnity-FrameRate" + this.frameRate + "-" + k + ".csv"))
        {
            k++;
        }
        //this.path = Application.dataPath + "/csv/fileTestNum" + k + ".csv";
        this.path = Application.dataPath + "../../../motionRugs-master/data/projetUnity-FrameRate" + this.frameRate + "-" + k + ".csv";

        // create file
        File.WriteAllText(this.path, "frame,id,x,y,speed,acceleration,distance_centroid,heading_change,frameRate,time\n");

        GameObject map=Instantiate(mapPrefab);
        map.transform.parent = null;
        map.transform.position = new Vector3(mapSizeX / 2.0f, 0.0f, mapSizeZ / 2.0f);
        map.transform.localScale = new Vector3(mapSizeX, 1.0f, mapSizeZ);

        mainCamera = FindObjectOfType<Camera>();
        mainCamera.transform.position= new Vector3(mapSizeX / 2.0f, Mathf.Max(mapSizeZ,mapSizeX), mapSizeZ / 2.0f);

        if (seedActive)
            random = new System.Random(seed);
        else
            random = new System.Random();

        agents = new List<GameObject>();
        for(int i=0; i<numberOfAgents; i++)
        {
            GameObject newAgent=GameObject.Instantiate(prefab);
            newAgent.transform.position = new Vector3((float) (random.NextDouble() * mapSizeX), 0.1f, (float) (random.NextDouble() * mapSizeZ));
            newAgent.transform.rotation = Quaternion.Euler(0.0f, (float) (random.NextDouble() * 359.0f), 0.0f);
            ReynoldsFlockingAgent rfa = newAgent.GetComponent<ReynoldsFlockingAgent>();
            rfa.setupParameters();
            rfa.SetFrameSave(0);
            rfa.SetID(i);
            agents.Add(newAgent);
        }
    }

    void FixedUpdate()
    {
        foreach (GameObject agent in agents)
        {
            ReynoldsFlockingAgent rfa = agent.GetComponent<ReynoldsFlockingAgent>();
            updateAgent(rfa);
            if (rfa.GetFrameSave() == 0) this.timeStart = System.Math.Truncate(Time.realtimeSinceStartup);
            if (Time.frameCount > 1)
            {
                if (Time.frameCount % frameRate == 0)
                {
                    this.adding += rfa.UpdateCSV() + "," + this.frameRate
                                + "," + (System.Math.Truncate(Time.realtimeSinceStartup) - this.timeStart)
                                + "\n";
                }
            }
            
        }
        /*
        for(int i = 0; i < agents.Count; i++)
        {
            GameObject agent = agents[i];
            int randomIndex = random.Next(i, agents.Count);
            agents[i] = agents[randomIndex];
            agents[randomIndex] = agent;
        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        File.AppendAllText(this.path, adding);
        adding = "";
    }

    public void updateAgent(ReynoldsFlockingAgent rfa)
    {
        rfa.decide();
        rfa.updateAgent();
    }

    public List<GameObject> GetAgents()
    {
        return agents;
    }

    public float GetMapSizeX()
    {
        return mapSizeX;
    }

    public float GetMapSizeZ()
    {
        return mapSizeZ;
    }

    public System.Random GetRandom()
    {
        return random;
    }
}
