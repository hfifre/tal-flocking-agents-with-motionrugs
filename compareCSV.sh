#!/bin/bash

csvSorted=$(du RobotSwarmSandbox-TAL/Assets/csv/*.csv | sort -h)
set -- $csvSorted
smallestFile=$2
comm -12 --nocheck-order ./RobotSwarmSandbox-TAL/Assets/csv/*0.csv ./RobotSwarmSandbox-TAL/Assets/csv/*1.csv > csvTests/fileTempo.csv

for file in ./RobotSwarmSandbox-TAL/Assets/csv/*.csv; do comm -12 --nocheck-order $file csvTests/fileTempo.csv > csvTests/fileSave.csv; cat csvTests/fileSave.csv > csvTests/fileTempo.csv; done

cat $smallestFile > csvTests/contentSmallFile.csv
res=$(comm -3 --nocheck-order csvTests/fileSave.csv csvTests/contentSmallFile.csv)
[[ -z "$res" ]] && echo "seed's working !" || echo "seed's not working..."